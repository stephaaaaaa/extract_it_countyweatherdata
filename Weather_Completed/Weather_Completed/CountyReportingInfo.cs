//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Weather_Completed
{
    using System;
    using System.Collections.Generic;
    
    public partial class CountyReportingInfo
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public CountyReportingInfo()
        {
            this.WeatherInfoes = new HashSet<WeatherInfo>();
        }
    
        public string County_Name { get; set; }
        public string Latitutde { get; set; }
        public string Longitude { get; set; }
        public string Point_Forecast { get; set; }
        public int Elevation_ft { get; set; }
        public System.DateTime Last_Updated { get; set; }
        public int ReportingInfo_ID { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<WeatherInfo> WeatherInfoes { get; set; }
    }
}
