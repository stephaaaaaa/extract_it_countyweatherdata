This project was developed for the purposes of storing the daily hourly forecast, for a 24-hour cycle, into a database.
The information stored contains data on counties, their respective reporting stations, latitude, and longitude. With this,
we can store and collect data that can then be used by the Public Works department.