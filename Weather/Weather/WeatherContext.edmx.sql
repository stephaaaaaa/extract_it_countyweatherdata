
-- --------------------------------------------------
-- Entity Designer DDL Script for SQL Server 2005, 2008, 2012 and Azure
-- --------------------------------------------------
-- Date Created: 06/29/2018 09:59:43
-- Generated from EDMX file: C:\Local Git Repo\Weather\Weather\Weather\WeatherContext.edmx
-- --------------------------------------------------

SET QUOTED_IDENTIFIER OFF;
GO
USE [Weather_DBFirst];
GO
IF SCHEMA_ID(N'dbo') IS NULL EXECUTE(N'CREATE SCHEMA [dbo]');
GO

-- --------------------------------------------------
-- Dropping existing FOREIGN KEY constraints
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[FK_WeatherInfo_CountyReportingInfo]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[WeatherInfo] DROP CONSTRAINT [FK_WeatherInfo_CountyReportingInfo];
GO

-- --------------------------------------------------
-- Dropping existing tables
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[CountyReportingInfo]', 'U') IS NOT NULL
    DROP TABLE [dbo].[CountyReportingInfo];
GO
IF OBJECT_ID(N'[dbo].[WeatherInfo]', 'U') IS NOT NULL
    DROP TABLE [dbo].[WeatherInfo];
GO

-- --------------------------------------------------
-- Creating all tables
-- --------------------------------------------------

-- Creating table 'CountyReportingInfoes'
CREATE TABLE [dbo].[CountyReportingInfoes] (
    [County_Name] nvarchar(50)  NOT NULL,
    [Latitutde] nvarchar(15)  NOT NULL,
    [Longitude] nvarchar(15)  NOT NULL,
    [Point_Forecast] nvarchar(75)  NOT NULL,
    [Elevation_ft] int  NOT NULL,
    [Last_Updated] datetime  NOT NULL,
    [ReportingInfo_ID] int IDENTITY(1,1) NOT NULL
);
GO

-- Creating table 'WeatherInfoes'
CREATE TABLE [dbo].[WeatherInfoes] (
    [Hour_MDT] int  NOT NULL,
    [Temperature_F] int  NOT NULL,
    [SurfaceWind_MPH] int  NOT NULL,
    [WindGustDir] nvarchar(5)  NOT NULL,
    [PrecipPotential_Percent] int  NOT NULL,
    [WeatherInfo_ID] int IDENTITY(1,1) NOT NULL,
    [ReportingInfo_ID] int  NOT NULL
);
GO

-- --------------------------------------------------
-- Creating all PRIMARY KEY constraints
-- --------------------------------------------------

-- Creating primary key on [ReportingInfo_ID] in table 'CountyReportingInfoes'
ALTER TABLE [dbo].[CountyReportingInfoes]
ADD CONSTRAINT [PK_CountyReportingInfoes]
    PRIMARY KEY CLUSTERED ([ReportingInfo_ID] ASC);
GO

-- Creating primary key on [WeatherInfo_ID] in table 'WeatherInfoes'
ALTER TABLE [dbo].[WeatherInfoes]
ADD CONSTRAINT [PK_WeatherInfoes]
    PRIMARY KEY CLUSTERED ([WeatherInfo_ID] ASC);
GO

-- --------------------------------------------------
-- Creating all FOREIGN KEY constraints
-- --------------------------------------------------

-- Creating foreign key on [ReportingInfo_ID] in table 'WeatherInfoes'
ALTER TABLE [dbo].[WeatherInfoes]
ADD CONSTRAINT [FK_WeatherInfo_CountyReportingInfo]
    FOREIGN KEY ([ReportingInfo_ID])
    REFERENCES [dbo].[CountyReportingInfoes]
        ([ReportingInfo_ID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_WeatherInfo_CountyReportingInfo'
CREATE INDEX [IX_FK_WeatherInfo_CountyReportingInfo]
ON [dbo].[WeatherInfoes]
    ([ReportingInfo_ID]);
GO

-- --------------------------------------------------
-- Script has ended
-- --------------------------------------------------