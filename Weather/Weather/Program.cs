﻿using HtmlAgilityPack;
using NLog;
using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.IO;
using System.Linq;

namespace Weather
{
    class Program
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();

        #region table links, with filters applied

        private static string canyonCountyTableURL = @"https://forecast.weather.gov/MapClick.php?w0=t&w3=sfcwind&w3u=1&w4=sky&w5=pop&w7=rain&w9=fog&AheadHour=0&Submit=Submit&FcstType=digital&textField1=43.6251&textField2=-116.7093&site=all&unit=0&dd=&bw=";
        private static string adaCountyTableURL = @"https://forecast.weather.gov/MapClick.php?w0=t&w3=sfcwind&w3u=1&w4=sky&w5=pop&w7=rain&w9=fog&AheadHour=0&Submit=Submit&FcstType=digital&textField1=43.4511&textField2=-116.2411&site=all&unit=0&dd=&bw=";
        private static string elmoreCountyTableURL = @"https://forecast.weather.gov/MapClick.php?w0=t&w3=sfcwind&w3u=1&w4=sky&w5=pop&w7=rain&w9=fog&AheadHour=0&Submit=Submit&FcstType=digital&textField1=43.354&textField2=-115.469&site=all&unit=0&dd=&bw=";
        private static string boiseCountyTableURL = @"https://forecast.weather.gov/MapClick.php?w0=t&w3=sfcwind&w3u=1&w4=sky&w5=pop&w7=rain&w9=fog&AheadHour=0&Submit=Submit&FcstType=digital&textField1=43.9892&textField2=-115.7303&site=all&unit=0&dd=&bw=";
        private static string gemCountyTableURL = @"https://forecast.weather.gov/MapClick.php?w0=t&w3=sfcwind&w3u=1&w4=sky&w5=pop&w7=rain&w9=fog&AheadHour=0&Submit=Submit&FcstType=digital&textField1=44.0616&textField2=-116.3973&site=all&unit=0&dd=&bw=";
        private static string camasCountyTableURL = @"https://forecast.weather.gov/MapClick.php?w0=t&w3=sfcwind&w3u=1&w4=sky&w5=pop&w7=rain&w9=fog&AheadHour=0&Submit=Submit&FcstType=digital&textField1=43.463&textField2=-114.8053&site=all&unit=0&dd=&bw=";

        #endregion

        #region county weather values lists

        private static List<int> canyonCountyHourVals;
        private static List<int> canyonCountyTemperatureValues;
        private static List<int> canyonCountySurfaceWindValues;
        private static List<string> canyonCountyWindDirectionValues;
        private static List<int> canyonCountySkyCoverValues;
        private static List<int> canyonCountyPrecipPotentialValues;
        private static List<DateTime> canyonCountyDateReporting;

        private static List<int> adaCountyHourVals;
        private static List<int> adaCountyTemperatureValues;
        private static List<int> adaCountySurfaceWindValues;
        private static List<string> adaCountyWindDirectionValues;
        private static List<int> adaCountySkyCoverValues;
        private static List<int> adaCountyPrecipPotentialValues;
        private static List<DateTime> adaCountyDateReporting;

        private static List<int> elmoreCountyHourVals;
        private static List<int> elmoreCountyTemperatureValues;
        private static List<int> elmoreCountySurfaceWindValues;
        private static List<string> elmoreCountyWindDirectionValues;
        private static List<int> elmoreCountySkyCoverValues;
        private static List<int> elmoreCountyPrecipPotentialValues;
        private static List<DateTime> elmoreCountyDateReporting;

        private static List<int> boiseCountyHourVals;
        private static List<int> boiseCountyTemperatureValues;
        private static List<int> boiseCountySurfaceWindValues;
        private static List<string> boiseCountyWindDirectionValues;
        private static List<int> boiseCountySkyCoverValues;
        private static List<int> boiseCountyPrecipPotentialValues;
        private static List<DateTime> boiseCountyDateReporting;

        private static List<int> gemCountyHourVals;
        private static List<int> gemCountyTemperatureValues;
        private static List<int> gemCountySurfaceWindValues;
        private static List<string> gemCountyWindDirectionValues;
        private static List<int> gemCountySkyCoverValues;
        private static List<int> gemCountyPrecipPotentialValues;
        private static List<DateTime> gemCountyDateReporting;

        private static List<int> camasCountyHourVals;
        private static List<int> camasCountyTemperatureValues;
        private static List<int> camasCountySurfaceWindValues;
        private static List<string> camasCountyWindDirectionValues;
        private static List<int> camasCountySkyCoverValues;
        private static List<int> camasCountyPrecipPotentialValues;
        private static List<DateTime> camasCountyDateReporting;

        #endregion

        #region county weather value helpers
        private static List<int> retrieveHourValues(string countyURL)
        {
            HtmlWeb web = new HtmlWeb();
            HtmlDocument doc = web.Load(countyURL);
            HtmlNode weatherDataTable = doc.DocumentNode.SelectSingleNode("//table[6]");
            HtmlNode temperatureRow = weatherDataTable.SelectSingleNode(".//tr[3]");
            List<int> hourValues = new List<int>();
            for (int index = 2; index < 26; index++)
            {
                HtmlNode hourCell = temperatureRow.SelectSingleNode($".//td[{index}]");
                int hour = Int32.Parse(hourCell.InnerText);
                hourValues.Add(hour);
            }

            return hourValues;
        }

        private static List<DateTime> retrieveDateReportedValues(string countyURL, List<int> hours)
        {
            HtmlWeb web = new HtmlWeb();
            HtmlDocument doc = web.Load(countyURL);
            HtmlNode weatherDataTable = doc.DocumentNode.SelectSingleNode("//table[6]");
            HtmlNodeCollection datesRow = weatherDataTable.SelectNodes(".//tr[2]");
            string datesIn24HourRange = datesRow.Single().InnerText;
            datesIn24HourRange = datesIn24HourRange.Replace("Date", "");
            string date1String = $"{datesIn24HourRange.Substring(0, 5)}/{DateTime.UtcNow.Year}";
            string date2String = $"{datesIn24HourRange.Substring(6, 4)}/{DateTime.UtcNow.Year}";
            DateTime date1 = DateTime.Parse(date1String);
            DateTime date2 = DateTime.Parse(date2String);
            List<DateTime> respectiveDates = new List<DateTime>();

            Boolean passed23Hours = false;
            foreach (int hour in hours)
            {
                if (hour == 23)
                {
                    respectiveDates.Add(date1);
                    passed23Hours = true;
                }
                else
                {
                    if (hour <= 23 && passed23Hours == false)
                    {
                        respectiveDates.Add(date1);
                    }
                    if (hour <= 23 && passed23Hours == true)
                    {
                        respectiveDates.Add(date2);
                    }
                }
            }
            return respectiveDates;
        }

        private static List<int> retrieveTemperatureValues(string countyURL)
        {
            HtmlWeb web = new HtmlWeb();
            HtmlDocument doc = web.Load(countyURL);
            HtmlNode weatherDataTable = doc.DocumentNode.SelectSingleNode("//table[6]");
            HtmlNode temperatureRow = weatherDataTable.SelectSingleNode(".//tr[4]");

            List<int> hourlyTemperatures = new List<int>();
            for (int index = 2; index < 26; index++)
            {
                HtmlNode temperatureCell = temperatureRow.SelectSingleNode($".//td[{index}]");
                int temperature = Int32.Parse(temperatureCell.InnerText);
                hourlyTemperatures.Add(temperature);
            }

            return hourlyTemperatures;
        }

        private static List<int> retrieveSurfaceWindValues(string countyURL)
        {
            HtmlWeb web = new HtmlWeb();
            HtmlDocument doc = web.Load(countyURL);
            HtmlNode weatherDataTable = doc.DocumentNode.SelectSingleNode("//table[6]");
            HtmlNode surfaceWindRow = weatherDataTable.SelectSingleNode(".//tr[5]");

            List<int> surfaceWind = new List<int>();
            for (int index = 2; index < 26; index++)
            {
                HtmlNode surfaceWindCell = surfaceWindRow.SelectSingleNode($".//td[{index}]");
                int surfaceWindVal = Int32.Parse(surfaceWindCell.InnerText);
                surfaceWind.Add(surfaceWindVal);
            }

            return surfaceWind;
        }

        private static List<string> retrieveWindDirectionValues(string countyURL)
        {
            HtmlWeb web = new HtmlWeb();
            HtmlDocument doc = web.Load(countyURL);
            HtmlNode weatherDataTable = doc.DocumentNode.SelectSingleNode("//table[6]");
            HtmlNode windDirectionRow = weatherDataTable.SelectSingleNode(".//tr[6]");

            List<string> windDirections = new List<string>();
            for (int index = 2; index < 26; index++)
            {
                HtmlNode windDirectionCell = windDirectionRow.SelectSingleNode($".//td[{index}]");
                string windDirectionValue = windDirectionCell.InnerText;
                windDirections.Add(windDirectionValue);
            }

            return windDirections;
        }

        private static List<int> retrieveSkyCoverValues(string countyURL)
        {
            HtmlWeb web = new HtmlWeb();
            HtmlDocument doc = web.Load(countyURL);
            HtmlNode weatherDataTable = doc.DocumentNode.SelectSingleNode("//table[6]");
            HtmlNode skyCoverRow = weatherDataTable.SelectSingleNode(".//tr[8]");

            List<int> skyCoverVals = new List<int>();
            for (int index = 2; index < 26; index++)
            {
                HtmlNode skyCoverCell = skyCoverRow.SelectSingleNode($".//td[{index}]");
                int skyCover = Int32.Parse(skyCoverCell.InnerText);
                skyCoverVals.Add(skyCover);
            }

            return skyCoverVals;
        }

        private static List<int> retrievePrecipPotentialValues(string countyURL)
        {
            HtmlWeb web = new HtmlWeb();
            HtmlDocument doc = web.Load(countyURL);
            HtmlNode weatherDataTable = doc.DocumentNode.SelectSingleNode("//table[6]");
            HtmlNode precipRow = weatherDataTable.SelectSingleNode(".//tr[9]");

            List<int> precipPotenValues = new List<int>();
            for (int index = 2; index < 26; index++)
            {
                HtmlNode precipPotenCell = precipRow.SelectSingleNode($".//td[{index}]");
                int precipPoten = Int32.Parse(precipPotenCell.InnerText);
                precipPotenValues.Add(precipPoten);
            }

            return precipPotenValues;
        }

        private static void populateCountyWeatherLists(string countyURL)
        {
            if (countyURL.Equals(canyonCountyTableURL))
            {
                canyonCountyHourVals = retrieveHourValues(canyonCountyTableURL);
                canyonCountyDateReporting = retrieveDateReportedValues(canyonCountyTableURL, canyonCountyHourVals);
                canyonCountyTemperatureValues = retrieveTemperatureValues(canyonCountyTableURL);
                canyonCountySurfaceWindValues = retrieveSurfaceWindValues(canyonCountyTableURL);
                canyonCountyWindDirectionValues = retrieveWindDirectionValues(canyonCountyTableURL);
                canyonCountySkyCoverValues = retrieveSkyCoverValues(canyonCountyTableURL);
                canyonCountyPrecipPotentialValues = retrievePrecipPotentialValues(canyonCountyTableURL);
            }
            if (countyURL.Equals(adaCountyTableURL))
            {
                adaCountyHourVals = retrieveHourValues(adaCountyTableURL);
                adaCountyDateReporting = retrieveDateReportedValues(adaCountyTableURL, adaCountyHourVals);
                adaCountyTemperatureValues = retrieveTemperatureValues(adaCountyTableURL);
                adaCountySurfaceWindValues = retrieveSurfaceWindValues(adaCountyTableURL);
                adaCountyWindDirectionValues = retrieveWindDirectionValues(adaCountyTableURL);
                adaCountySkyCoverValues = retrieveSkyCoverValues(adaCountyTableURL);
                adaCountyPrecipPotentialValues = retrievePrecipPotentialValues(adaCountyTableURL);
            }
            if (countyURL.Equals(elmoreCountyTableURL))
            {
                elmoreCountyHourVals = retrieveHourValues(elmoreCountyTableURL);
                elmoreCountyDateReporting = retrieveDateReportedValues(elmoreCountyTableURL, elmoreCountyHourVals);
                elmoreCountyTemperatureValues = retrieveTemperatureValues(elmoreCountyTableURL);
                elmoreCountySurfaceWindValues = retrieveSurfaceWindValues(elmoreCountyTableURL);
                elmoreCountyWindDirectionValues = retrieveWindDirectionValues(elmoreCountyTableURL);
                elmoreCountySkyCoverValues = retrieveSkyCoverValues(elmoreCountyTableURL);
                elmoreCountyPrecipPotentialValues = retrievePrecipPotentialValues(elmoreCountyTableURL);
            }
            if (countyURL.Equals(boiseCountyTableURL))
            {
                boiseCountyHourVals = retrieveHourValues(boiseCountyTableURL);
                boiseCountyDateReporting = retrieveDateReportedValues(boiseCountyTableURL, boiseCountyHourVals);
                boiseCountyTemperatureValues = retrieveTemperatureValues(boiseCountyTableURL);
                boiseCountySurfaceWindValues = retrieveSurfaceWindValues(boiseCountyTableURL);
                boiseCountyWindDirectionValues = retrieveWindDirectionValues(boiseCountyTableURL);
                boiseCountySkyCoverValues = retrieveSkyCoverValues(boiseCountyTableURL);
                boiseCountyPrecipPotentialValues = retrievePrecipPotentialValues(boiseCountyTableURL);
            }
            if (countyURL.Equals(gemCountyTableURL))
            {
                gemCountyHourVals = retrieveHourValues(gemCountyTableURL);
                gemCountyDateReporting = retrieveDateReportedValues(gemCountyTableURL, gemCountyHourVals);
                gemCountyTemperatureValues = retrieveTemperatureValues(gemCountyTableURL);
                gemCountySurfaceWindValues = retrieveSurfaceWindValues(gemCountyTableURL);
                gemCountyWindDirectionValues = retrieveWindDirectionValues(gemCountyTableURL);
                gemCountySkyCoverValues = retrieveSkyCoverValues(gemCountyTableURL);
                gemCountyPrecipPotentialValues = retrievePrecipPotentialValues(gemCountyTableURL);
            }
            if (countyURL.Equals(camasCountyTableURL))
            {
                camasCountyHourVals = retrieveHourValues(camasCountyTableURL);
                camasCountyDateReporting = retrieveDateReportedValues(camasCountyTableURL, camasCountyHourVals);
                camasCountyTemperatureValues = retrieveTemperatureValues(camasCountyTableURL);
                camasCountySurfaceWindValues = retrieveSurfaceWindValues(camasCountyTableURL);
                camasCountyWindDirectionValues = retrieveWindDirectionValues(camasCountyTableURL);
                camasCountySkyCoverValues = retrieveSkyCoverValues(camasCountyTableURL);
                camasCountyPrecipPotentialValues = retrievePrecipPotentialValues(camasCountyTableURL);
            }
        }

        private static void populateAllCountiesWeatherLists()
        {
            logger.Info("Populating weather values for Canyon County Weather ...");
            populateCountyWeatherLists(canyonCountyTableURL);

            logger.Info("Populating weather values for Ada County Weather ...");
            populateCountyWeatherLists(adaCountyTableURL);

            logger.Info("Populating weather values for Elmore County Weather ...");
            populateCountyWeatherLists(elmoreCountyTableURL);

            logger.Info("Populating weather values for Boise County Weather ...");
            populateCountyWeatherLists(boiseCountyTableURL);

            logger.Info("Populating weather values for Gem County Weather ...");
            populateCountyWeatherLists(gemCountyTableURL);

            logger.Info("Populating Camas values for Camas County Weather ...");
            populateCountyWeatherLists(camasCountyTableURL);
        }
        #endregion

        #region county info lists

        private static string canyonCountyName = "Canyon County";
        private static string canyonLatitude;
        private static string canyonLongitude;
        private static string canyonPointForecast;
        private static int canyonElevationft;
        private static DateTime canyonLastUpdated;

        private static string adaCountyName = "Ada County";
        private static string adaLatitude;
        private static string adaLongitude;
        private static string adaPointForecast;
        private static int adaElevationft;
        private static DateTime adaLastUpdated;

        private static string elmoreCountyName = "Elmore County";
        private static string elmoreLatitude;
        private static string elmoreLongitude;
        private static string elmorePointForecast;
        private static int elmoreElevationft;
        private static DateTime elmoreLastUpdated;

        private static string boiseCountyName = "Boise County";
        private static string boiseLatitude;
        private static string boiseLongitude;
        private static string boisePointForecast;
        private static int boiseElevationft;
        private static DateTime boiseLastUpdated;

        private static string gemCountyName = "Gem County";
        private static string gemLatitude;
        private static string gemLongitude;
        private static string gemPointForecast;
        private static int gemElevationft;
        private static DateTime gemLastUpdated;

        private static string camasCountyName = "Camas County";
        private static string camasLatitude;
        private static string camasLongitude;
        private static string camasPointForecast;
        private static int camasElevationft;
        private static DateTime camasLastUpdated;

        #endregion

        #region county information value helpers

        private static DateTime parseDateTimeString(string lastUpdated)
        {
            lastUpdated = lastUpdated.Substring(12, lastUpdated.Length - 12);
            lastUpdated = lastUpdated.Trim();
            lastUpdated = lastUpdated.Replace("MDT ", "");
            DateTime lastUpdatedDate = DateTime.Parse(lastUpdated);

            return lastUpdatedDate;
        }

        private static void parseCanyonHeaderInformation(string headerRow)
        {
            headerRow = headerRow.Replace("&nbsp", " ").Trim();
            string[] countyInfoValues = headerRow.Split(';');
            for (int i = 0; i < countyInfoValues.Length; i++)
            {
                string current = countyInfoValues[i];
                if (current.Contains("Point Forecast")) // only point forecast
                { // parse up to after 'point forecast'
                    countyInfoValues[i] = current.Substring(15, current.Length - 15);
                    canyonPointForecast = countyInfoValues[i];
                }
                if (current.Contains("Elev")) // will be "lat long (Elev. elevation)"
                {
                    string[] latAndLongAndElev = current.Split(' ');
                    canyonLatitude = latAndLongAndElev[0];
                    canyonLongitude = latAndLongAndElev[1];
                    canyonElevationft = Int32.Parse(latAndLongAndElev[4]);
                }
            }
        }

        private static void parseAdaHeaderInformation(string headerRow)
        {
            headerRow = headerRow.Replace("&nbsp", " ").Trim();
            string[] countyInfoValues = headerRow.Split(';');
            for (int i = 0; i < countyInfoValues.Length; i++)
            {
                string current = countyInfoValues[i];
                if (current.Contains("Point Forecast")) // only point forecast
                { // parse up to after 'point forecast'
                    countyInfoValues[i] = current.Substring(15, current.Length - 15);
                    adaPointForecast = countyInfoValues[i];
                }
                if (current.Contains("Elev")) // will be "lat long (Elev. elevation)"
                {
                    string[] latAndLongAndElev = current.Split(' ');
                    adaLatitude = latAndLongAndElev[0];
                    adaLongitude = latAndLongAndElev[1];
                    adaElevationft = Int32.Parse(latAndLongAndElev[4]);
                }
            }
        }

        private static void parseElmoreHeaderInformation(string headerRow)
        {
            headerRow = headerRow.Replace("&nbsp", " ").Trim();
            string[] countyInfoValues = headerRow.Split(';');
            for (int i = 0; i < countyInfoValues.Length; i++)
            {
                string current = countyInfoValues[i];
                if (current.Contains("Point Forecast")) // only point forecast
                { // parse up to after 'point forecast'
                    countyInfoValues[i] = current.Substring(15, current.Length - 15);
                    elmorePointForecast = countyInfoValues[i];
                }
                if (current.Contains("Elev")) // will be "lat long (Elev. elevation)"
                {
                    string[] latAndLongAndElev = current.Split(' ');
                    elmoreLatitude = latAndLongAndElev[0];
                    elmoreLongitude = latAndLongAndElev[1];
                    elmoreElevationft = Int32.Parse(latAndLongAndElev[4]);
                }
            }
        }

        private static void parseBoiseHeaderInformation(string headerRow)
        {
            headerRow = headerRow.Replace("&nbsp", " ").Trim();
            string[] countyInfoValues = headerRow.Split(';');
            for (int i = 0; i < countyInfoValues.Length; i++)
            {
                string current = countyInfoValues[i];
                if (current.Contains("Point Forecast")) // only point forecast
                { // parse up to after 'point forecast'
                    countyInfoValues[i] = current.Substring(15, current.Length - 15);
                    boisePointForecast = countyInfoValues[i];
                }
                if (current.Contains("Elev")) // will be "lat long (Elev. elevation)"
                {
                    string[] latAndLongAndElev = current.Split(' ');
                    boiseLatitude = latAndLongAndElev[0];
                    boiseLongitude = latAndLongAndElev[1];
                    boiseElevationft = Int32.Parse(latAndLongAndElev[4]);
                }
            }
        }

        private static void parseGemHeaderInformation(string headerRow)
        {
            headerRow = headerRow.Replace("&nbsp", " ").Trim();
            string[] countyInfoValues = headerRow.Split(';');
            for (int i = 0; i < countyInfoValues.Length; i++)
            {
                string current = countyInfoValues[i];
                if (current.Contains("Point Forecast")) // only point forecast
                { // parse up to after 'point forecast'
                    countyInfoValues[i] = current.Substring(15, current.Length - 15);
                    gemPointForecast = countyInfoValues[i];
                }
                if (current.Contains("Elev")) // will be "lat long (Elev. elevation)"
                {
                    string[] latAndLongAndElev = current.Split(' ');
                    gemLatitude = latAndLongAndElev[0];
                    gemLongitude = latAndLongAndElev[1];
                    gemElevationft = Int32.Parse(latAndLongAndElev[4]);
                }
            }
        }

        private static void parseCamasHeaderInformation(string headerRow)
        {
            headerRow = headerRow.Replace("&nbsp", " ").Trim();
            string[] countyInfoValues = headerRow.Split(';');
            for (int i = 0; i < countyInfoValues.Length; i++)
            {
                string current = countyInfoValues[i];
                if (current.Contains("Point Forecast")) // only point forecast
                { // parse up to after 'point forecast'
                    countyInfoValues[i] = current.Substring(15, current.Length - 15);
                    camasPointForecast = countyInfoValues[i];
                }
                if (current.Contains("Elev")) // will be "lat long (Elev. elevation)"
                {
                    string[] latAndLongAndElev = current.Split(' ');
                    camasLatitude = latAndLongAndElev[0];
                    camasLongitude = latAndLongAndElev[1];
                    camasElevationft = Int32.Parse(latAndLongAndElev[4]);
                }
            }
        }

        private static void setCountyReportingInfo(string countyURL)
        {
            HtmlWeb web = new HtmlWeb();
            HtmlDocument doc = web.Load(countyURL);
            HtmlNode weatherDataTable = doc.DocumentNode.SelectSingleNode("//table[4]");
            HtmlNode pointForecastHeader = weatherDataTable.SelectSingleNode(".//tr[1]");

            if (countyURL.Equals(canyonCountyTableURL))
            {
                string canyonInfoRow = pointForecastHeader.SelectSingleNode(".//td[1]").InnerText.Trim();
                parseCanyonHeaderInformation(canyonInfoRow);

                string lastUpdated = pointForecastHeader.SelectSingleNode(".//td[2]").InnerText;
                DateTime lastUpdatedDate = parseDateTimeString(lastUpdated);
                canyonLastUpdated = lastUpdatedDate;

            }
            if (countyURL.Equals(adaCountyTableURL))
            {
                string adaInfoRow = pointForecastHeader.SelectSingleNode(".//td[1]").InnerText.Trim();
                parseAdaHeaderInformation(adaInfoRow);

                string lastUpdated = pointForecastHeader.SelectSingleNode(".//td[2]").InnerText;
                DateTime lastUpdatedDate = parseDateTimeString(lastUpdated);
                adaLastUpdated = lastUpdatedDate;
            }
            if (countyURL.Equals(elmoreCountyTableURL))
            {
                string elmoreInfoRow = pointForecastHeader.SelectSingleNode(".//td[1]").InnerText.Trim();
                parseElmoreHeaderInformation(elmoreInfoRow);

                string lastUpdated = pointForecastHeader.SelectSingleNode(".//td[2]").InnerText;
                DateTime lastUpdatedDate = parseDateTimeString(lastUpdated);
                elmoreLastUpdated = lastUpdatedDate;
            }
            if (countyURL.Equals(boiseCountyTableURL))
            {
                string boiseInfoRow = pointForecastHeader.SelectSingleNode(".//td[1]").InnerText.Trim();
                parseBoiseHeaderInformation(boiseInfoRow);

                string lastUpdated = pointForecastHeader.SelectSingleNode(".//td[2]").InnerText;
                DateTime lastUpdatedDate = parseDateTimeString(lastUpdated);
                boiseLastUpdated = lastUpdatedDate;
            }
            if (countyURL.Equals(gemCountyTableURL))
            {
                string gemInfoRow = pointForecastHeader.SelectSingleNode(".//td[1]").InnerText.Trim();
                parseGemHeaderInformation(gemInfoRow);

                string lastUpdated = pointForecastHeader.SelectSingleNode(".//td[2]").InnerText;
                DateTime lastUpdatedDate = parseDateTimeString(lastUpdated);
                gemLastUpdated = lastUpdatedDate;
            }
            if (countyURL.Equals(camasCountyTableURL))
            {
                string camasInfoRow = pointForecastHeader.SelectSingleNode(".//td[1]").InnerText.Trim();
                parseCamasHeaderInformation(camasInfoRow);

                string lastUpdated = pointForecastHeader.SelectSingleNode(".//td[2]").InnerText;
                DateTime lastUpdatedDate = parseDateTimeString(lastUpdated);
                camasLastUpdated = lastUpdatedDate;
            }
        }

        private static void setAllCountiesReportingInfo()
        {
            setCountyReportingInfo(canyonCountyTableURL);
            setCountyReportingInfo(adaCountyTableURL);
            setCountyReportingInfo(elmoreCountyTableURL);
            setCountyReportingInfo(boiseCountyTableURL);
            setCountyReportingInfo(gemCountyTableURL);
            setCountyReportingInfo(camasCountyTableURL);
        }

        #endregion

        private static void populateWeatherAndReportingForAllCounties()
        {
            logger.Info("Populating all counties' weather info lists:");
            populateAllCountiesWeatherLists();
            logger.Info("Populated county temperature values!");

            logger.Info("Populating all counties' reporting info lists");
            setAllCountiesReportingInfo();
            logger.Info("Populated county reporting values!");
        }

        private static CountyReportingInfo makeCountyInfoObj(string countyName)
        {
            CountyReportingInfo newCounty = new CountyReportingInfo();
            if (countyName.Equals(canyonCountyName))
            {
                newCounty.County_Name = canyonCountyName;
                newCounty.Latitutde = canyonLatitude;
                newCounty.Longitude = canyonLongitude;
                newCounty.Point_Forecast = canyonPointForecast;
                newCounty.Elevation_ft = canyonElevationft;
                newCounty.Last_Updated = canyonLastUpdated;
            }
            if (countyName.Equals(adaCountyName))
            {
                newCounty.County_Name = adaCountyName;
                newCounty.Latitutde = adaLatitude;
                newCounty.Longitude = adaLongitude;
                newCounty.Point_Forecast = adaPointForecast;
                newCounty.Elevation_ft = adaElevationft;
                newCounty.Last_Updated = adaLastUpdated;
            }
            if (countyName.Equals(elmoreCountyName))
            {
                newCounty.County_Name = elmoreCountyName;
                newCounty.Latitutde = elmoreLatitude;
                newCounty.Longitude = elmoreLongitude;
                newCounty.Point_Forecast = elmorePointForecast;
                newCounty.Elevation_ft = elmoreElevationft;
                newCounty.Last_Updated = elmoreLastUpdated;
            }
            if (countyName.Equals(boiseCountyName))
            {
                newCounty.County_Name = boiseCountyName;
                newCounty.Latitutde = boiseLatitude;
                newCounty.Longitude = boiseLongitude;
                newCounty.Point_Forecast = boisePointForecast;
                newCounty.Elevation_ft = boiseElevationft;
                newCounty.Last_Updated = boiseLastUpdated;
            }
            if (countyName.Equals(gemCountyName))
            {
                newCounty.County_Name = gemCountyName;
                newCounty.Latitutde = gemLatitude;
                newCounty.Longitude = gemLongitude;
                newCounty.Point_Forecast = gemPointForecast;
                newCounty.Elevation_ft = gemElevationft;
                newCounty.Last_Updated = gemLastUpdated;
            }
            if (countyName.Equals(camasCountyName))
            {
                newCounty.County_Name = camasCountyName;
                newCounty.Latitutde = camasLatitude;
                newCounty.Longitude = camasLongitude;
                newCounty.Point_Forecast = camasPointForecast;
                newCounty.Elevation_ft = camasElevationft;
                newCounty.Last_Updated = camasLastUpdated;
            }
            return newCounty;
        }


        static void Main(string[] args)
        {
            string location = Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);
            logger.Info("Start");
            logger.Info("Location: {0}", location);

            populateWeatherAndReportingForAllCounties();
            List<CountyReportingInfo> countiesInfo = new List<CountyReportingInfo>();
            List<WeatherInfo> weatherInfo = new List<WeatherInfo>();

            logger.Info("Attempting to populate db ...");
            using (var db = new Weather_DBFirstEntities())
            {
                CountyReportingInfo canyonInfo = makeCountyInfoObj(canyonCountyName);
                countiesInfo.Add(canyonInfo);

                CountyReportingInfo adaInfo = makeCountyInfoObj(adaCountyName);
                countiesInfo.Add(adaInfo);

                CountyReportingInfo elmoreInfo = makeCountyInfoObj(elmoreCountyName);
                countiesInfo.Add(elmoreInfo);

                CountyReportingInfo boiseInfo = makeCountyInfoObj(boiseCountyName);
                countiesInfo.Add(boiseInfo);

                CountyReportingInfo gemInfo = makeCountyInfoObj(gemCountyName);
                countiesInfo.Add(gemInfo);

                CountyReportingInfo camasInfo = makeCountyInfoObj(camasCountyName);
                countiesInfo.Add(camasInfo);


                for (int i = 0; i < 24; i++)
                {
                    try
                    {
                        WeatherInfo makeWeatherInfoObj(string countyName)
                        {
                            WeatherInfo newCountyWeather = new WeatherInfo();
                            if (countyName.Equals(canyonCountyName))
                            {
                                newCountyWeather.Hour_MDT = canyonCountyHourVals[i];
                                newCountyWeather.Date = canyonCountyDateReporting[i];
                                newCountyWeather.Temperature_F = canyonCountyTemperatureValues[i];
                                newCountyWeather.SurfaceWind_MPH = canyonCountySurfaceWindValues[i];
                                newCountyWeather.WindGustDir = canyonCountyWindDirectionValues[i];
                                newCountyWeather.PrecipPotential_Percent = canyonCountyPrecipPotentialValues[i];
                                newCountyWeather.CountyReportingInfo = canyonInfo;
                                newCountyWeather.WeatherInfo_ID = i;
                                newCountyWeather.ReportingInfo_ID = i;
                            }
                            if (countyName.Equals(adaCountyName))
                            {
                                newCountyWeather.Hour_MDT = adaCountyHourVals[i];
                                newCountyWeather.Date = adaCountyDateReporting[i];
                                newCountyWeather.Temperature_F = adaCountyTemperatureValues[i];
                                newCountyWeather.SurfaceWind_MPH = adaCountySurfaceWindValues[i];
                                newCountyWeather.WindGustDir = adaCountyWindDirectionValues[i];
                                newCountyWeather.PrecipPotential_Percent = adaCountyPrecipPotentialValues[i];
                                newCountyWeather.CountyReportingInfo = adaInfo;
                                newCountyWeather.WeatherInfo_ID = i;
                                newCountyWeather.ReportingInfo_ID = i;
                            }
                            if (countyName.Equals(elmoreCountyName))
                            {
                                newCountyWeather.Hour_MDT = elmoreCountyHourVals[i];
                                newCountyWeather.Date = elmoreCountyDateReporting[i];
                                newCountyWeather.Temperature_F = elmoreCountyTemperatureValues[i];
                                newCountyWeather.SurfaceWind_MPH = elmoreCountySurfaceWindValues[i];
                                newCountyWeather.WindGustDir = elmoreCountyWindDirectionValues[i];
                                newCountyWeather.PrecipPotential_Percent = elmoreCountyPrecipPotentialValues[i];
                                newCountyWeather.CountyReportingInfo = elmoreInfo;
                                newCountyWeather.WeatherInfo_ID = i;
                                newCountyWeather.ReportingInfo_ID = i;
                            }
                            if (countyName.Equals(boiseCountyName))
                            {
                                newCountyWeather.Hour_MDT = boiseCountyHourVals[i];
                                newCountyWeather.Date = boiseCountyDateReporting[i];
                                newCountyWeather.Temperature_F = boiseCountyTemperatureValues[i];
                                newCountyWeather.SurfaceWind_MPH = boiseCountySurfaceWindValues[i];
                                newCountyWeather.WindGustDir = boiseCountyWindDirectionValues[i];
                                newCountyWeather.PrecipPotential_Percent = boiseCountyPrecipPotentialValues[i];
                                newCountyWeather.CountyReportingInfo = boiseInfo;
                                newCountyWeather.WeatherInfo_ID = i;
                                newCountyWeather.ReportingInfo_ID = i;
                            }
                            if (countyName.Equals(gemCountyName))
                            {
                                newCountyWeather.Hour_MDT = gemCountyHourVals[i];
                                newCountyWeather.Date = gemCountyDateReporting[i];
                                newCountyWeather.Temperature_F = gemCountyTemperatureValues[i];
                                newCountyWeather.SurfaceWind_MPH = gemCountySurfaceWindValues[i];
                                newCountyWeather.WindGustDir = gemCountyWindDirectionValues[i];
                                newCountyWeather.PrecipPotential_Percent = gemCountyPrecipPotentialValues[i];
                                newCountyWeather.CountyReportingInfo = gemInfo;
                                newCountyWeather.WeatherInfo_ID = i;
                                newCountyWeather.ReportingInfo_ID = i;
                            }
                            if (countyName.Equals(camasCountyName))
                            {
                                newCountyWeather.Hour_MDT = camasCountyHourVals[i];
                                newCountyWeather.Date = camasCountyDateReporting[i];
                                newCountyWeather.Temperature_F = camasCountyTemperatureValues[i];
                                newCountyWeather.SurfaceWind_MPH = camasCountySurfaceWindValues[i];
                                newCountyWeather.WindGustDir = camasCountyWindDirectionValues[i];
                                newCountyWeather.PrecipPotential_Percent = camasCountyPrecipPotentialValues[i];
                                newCountyWeather.CountyReportingInfo = camasInfo;
                                newCountyWeather.WeatherInfo_ID = i;
                                newCountyWeather.ReportingInfo_ID = i;
                            }
                            return newCountyWeather;
                        }
                        WeatherInfo canyonWeatherInfo = makeWeatherInfoObj(canyonCountyName);
                        weatherInfo.Add(canyonWeatherInfo);
                        WeatherInfo adaWeatherInfo = makeWeatherInfoObj(adaCountyName);
                        weatherInfo.Add(adaWeatherInfo);
                        WeatherInfo elmoreWeatherInfo = makeWeatherInfoObj(elmoreCountyName);
                        weatherInfo.Add(elmoreWeatherInfo);
                        WeatherInfo boiseWeatherInfo = makeWeatherInfoObj(boiseCountyName);
                        weatherInfo.Add(boiseWeatherInfo);
                        WeatherInfo gemWeatherInfo = makeWeatherInfoObj(gemCountyName);
                        weatherInfo.Add(gemWeatherInfo);
                        WeatherInfo camasWeatherInfo = makeWeatherInfoObj(camasCountyName);
                        weatherInfo.Add(camasWeatherInfo);
                    }
                    catch (Exception e)
                    {
                        logger.Error(e, e.Message);
                    }

                }
                foreach (WeatherInfo weather in weatherInfo)
                    try
                    {
                        db.WeatherInfoes.AddOrUpdate(weather);
                    }
                    catch (Exception e)
                    {
                        logger.Error(e, e.Message);
                    }


                foreach (CountyReportingInfo county in countiesInfo)
                {
                    try
                    {
                        db.CountyReportingInfoes.AddOrUpdate(county);
                    }
                    catch (Exception e)
                    {
                        logger.Error(e, e.Message);
                    }
                }
                db.SaveChanges();
            }

            // end
            logger.Info("Finish");
            Console.Read();
        }
    }
}
