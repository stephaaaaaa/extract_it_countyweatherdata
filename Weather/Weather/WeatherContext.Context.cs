﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Weather
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Infrastructure;
    
    public partial class Weather_DBFirstEntities : DbContext
    {
        public Weather_DBFirstEntities()
            : base("name=Weather_DBFirstEntities")
        {
        }
    
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            throw new UnintentionalCodeFirstException();
        }
    
        public virtual DbSet<CountyReportingInfo> CountyReportingInfoes { get; set; }
        public virtual DbSet<WeatherInfo> WeatherInfoes { get; set; }
    }
}
